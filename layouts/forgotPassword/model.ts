/**
 * Specify required object
 *
 * @examples require(".").sampleData
 */
export interface IModel {
  firstName: string;
  returnUrl: string;
}

export const sampleData: IModel[] = [
  {
    firstName: 'test',
    returnUrl: 'https://www.rocketmakers.com',
  },
  {
    firstName: 'test2',
    returnUrl: 'https://www.rocketmakers.com',
  },
];
